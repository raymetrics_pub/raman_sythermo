""" Calculation of the rotational Raman scattering of N2 and O2 molecules.
"""
import numpy as np
from matplotlib import pyplot as plt

# Physical constants taken from http://physics.nist.gov/cuu/Constants/index.html (2014 values)
h = 6.626070040e-34  # plank constant in J s
c = 299792458.  # speed of light in m s-1
k_b = 1.38064852 * 10**-23  # Boltzmann constant in J/K

# plank constant * speed of light in cm * J
hc = h * c * 100  # cm * J

# plank constant * speed of light / Boltzmann constant in cm * K
hc_k = h * c / k_b * 100  # cm * K


class MolecularRamanScattering(object):

    def __init__(self, incident_wavelength, temperature, name, B0, D0, I, gamma_square, g,
                 relative_concentration, max_J=40):
        """
        Rotational Raman scattering of a generic molecule.

        Parameters
        ----------
        incident_wavelength : float
           Incident wavelength (nm)
        temperature : float
           Ambient temperature (K)
        name : str
           Molecule name, to be used in plots. E.g.'O_{2}'
        B0 : float
           Rotational constant (cm^{-1})
        D0 : float
           Centrifugal distortion constant (cm^{-1})
        I : int
           Nuclear spin
        gamma_square : float or callable
           Molecular polarization tensor (cm^{6})
        g : list of ints
           Statistical weight factor for even and odd J
        relative_concentration : float
           Relative concentration of the gas in the atmosphere (0 to 1)
        max_J : int
           Maximum quantum number to use in calculations.
        """
        self.incident_wavelength = incident_wavelength
        self.wavenumber = 1e7 / incident_wavelength

        self.temperature = temperature
        self.name = name
        self.B0 = B0
        self.D0 = D0
        self.I = I

        # Check if callable or just a number
        if callable(gamma_square):
            self.gamma_square = gamma_square(self.wavenumber)
        else:
            self.gamma_square = gamma_square  # Assume a float is provided

        self.g = g
        self.relative_concentration = relative_concentration
        self.max_J = max_J

        self.J_stokes = np.arange(0, max_J)
        self.J_astokes = np.arange(2, max_J)

        self.ds_stokes = [self.cross_section_stokes(J) for J in self.J_stokes]
        self.ds_astokes = [self.cross_section_antistokes(J) for J in self.J_astokes]

        self.dn_stokes = np.array([self.raman_shift_stokes(J) for J in self.J_stokes])
        self.dn_astokes = np.array([self.raman_shift_antistokes(J) for J in self.J_astokes])

        self.dl_stokes = 1 / (1 / incident_wavelength + np.array(self.dn_stokes) * 10 ** -7)
        self.dl_astokes = 1 / (1 / incident_wavelength + np.array(self.dn_astokes) * 10 ** -7)

    def rotational_energy(self, J):
        """ Rotational energy of a homonuclear diatomic molecule for quantum number J.

        Parameters
        ----------
        J : int
           Rotational quantum number.

        Returns
        -------
        E_rot : float
           Rotational energy of the molecule (J)
        """
        E_rot = (self.B0 * J * (J + 1) - self.D0 * J ** 2 * (J + 1) ** 2) * hc
        return E_rot

    def raman_shift_stokes(self, J):
        """ Calculates the rotational Raman shift  (delta_n) for the Stokes branch for
        quantum number J.

        Parameters
        ----------
        J : int
           Rotational quantum number

        Returns
        -------
        delta_n: float
           Rotational Raman shift (cm-1)
        """
        delta_n = -self.B0 * 2 * (2 * J + 3) + self.D0 * (3 * (2 * J + 3) + (2 * J + 3) ** 3)
        return delta_n

    def raman_shift_antistokes(self, J):
        """ Calculates the rotational Raman shift (delta_n) for the anti-Stokes branch for
        quantum number J.

        Parameters
        ----------
        J: int
           Rotational quantum number

        Returns
        -------
        delta_n: float
           Rotational Raman shift (cm-1)
        """
        delta_n = self.B0 * 2 * (2 * J - 1) - self.D0 * (3 * (2 * J - 1) + (2 * J - 1) ** 3)
        return delta_n

    def cross_section_stokes(self, J):
        """ Calculates the rotational Raman backsattering cross section for the Stokes
        branch for quantum number J at a temperature T.

        Parameters
        ----------
        J : int
           Rotational quantum number

        Returns
        -------
        b_s : float
           Scattering cross section (cm^{2}sr^{-1})
        """
        g_index = np.remainder(J, 2)
        g = self.g[g_index]

        raman_shift = self.raman_shift_stokes(J)
        rotational_energy = self.rotational_energy(J)

        b_s = 64 * np.pi ** 4 * hc_k / 15
        b_s *= g * self.B0 * (self.wavenumber + raman_shift) ** 4 * self.gamma_square
        b_s /= (2 * self.I + 1) ** 2 * self.temperature
        b_s *= (J + 1) * (J + 2) / (2 * J + 3)
        b_s *= np.exp(-rotational_energy / (k_b * self.temperature))
        return b_s

    def cross_section_antistokes(self, J):
        """ Calculates the rotational Raman backsattering cross section for the Stokes
        branch for quantum number J at a temperature T.

        Parameters
        ----------
        J : int
           Rotational quantum number

        Returns
        -------
        b_s : float
           Scattering cross section (cm^{2}sr^{-1})
        """
        g_index = np.remainder(J, 2)
        g = self.g[g_index]

        raman_shift = self.raman_shift_antistokes(J)
        rotational_energy = self.rotational_energy(J)

        b_s = 64 * np.pi ** 4 * hc_k / 15.

        b_s *= g * self.B0 * (self.wavenumber + raman_shift) ** 4 * self.gamma_square
        b_s /= (2 * self.I + 1) ** 2 * self.temperature
        b_s *= J * (J - 1) / (2 * J - 1)
        b_s *= np.exp(-rotational_energy / (k_b * self.temperature))
        return b_s

    def plot_cross_section(self, figsize=(10, 5), color='blue', grid=False, y_max=None):
        """
        Plot the molecular spectrum.

        Parameters
        ----------
        figsize : tuple
           Figure size (width, height)
        color : str
           Color of bars in the plot
        grid : bool
           If True, draw grid.
        y_max : tuple of floats or None
           If provided, it sets the maximum y of the plot
        """
        fig = plt.figure(figsize=figsize)
        ax = fig.add_subplot(111)

        self.draw_cross_section(ax, color=color, grid=grid, y_max=y_max)

        fig.suptitle('Rotational Raman backscattering cross-section of ${0}$'.format(self.name))

        plt.draw()
        plt.show()
        return fig, ax

    def draw_cross_section(self, ax, color='blue', grid=False, y_max=None):
        """
        Draw rotational Raman spectrum on a given axis.

        Parameters
        ----------
        ax : plt.Axes
           Figure axes
        color : str
           Color of bars in the plot
        grid : bool
           If True, draw grid.
        y_max : tuple of floats or None
           If provided, it sets the maximum y of the plot
        """
        x_step = self.dl_stokes[1] - self.dl_stokes[0]
        bar_width = x_step / 2.

        bar1 = ax.bar(self.dl_stokes, self.ds_stokes, width=bar_width, color=color)
        _ = ax.bar(self.dl_astokes, self.ds_astokes, width=bar_width, color=color)

        ax.set_xlabel('Wavelength [nm])')
        ax.set_ylabel(r'$\left( \frac{d\sigma}{d\omega}\right)_{\pi}$ [$cm^{2}sr^{-1}$]')

        if y_max:
            ax.set_ylim(0, y_max)

        ax.grid(grid)

        ax.legend([bar1[0], ], ['Cross section', ], loc=1)


class N2RamanScattering(MolecularRamanScattering):

    def __init__(self, incident_wavelength, temperature, max_J=40):
        """
        Rotational Raman scattering of N2.

        Parameters
        ----------
        incident_wavelength : float
           Incident wavelength (nm)
        temperature : float
           Ambient temperature (K)
        max_J : int
           Maximum quantum number to use in calculations.
        """
        super(N2RamanScattering, self).__init__(incident_wavelength,
                                                temperature,
                                                name='N_{2}',
                                                B0=1.989500,
                                                D0=5.48E-6,
                                                I=1,
                                                gamma_square=self.gamma_square_function,
                                                g=[6, 3],
                                                relative_concentration=0.79,
                                                max_J=max_J)

    @staticmethod
    def gamma_square_function(wavenumber, ignore_range=False):
        """ Returns the gamma squared parameter for N2 for a given wavelength.

        The empirical fit is take from:

        Chance, K. V. & Spurr, R. J. D. Ring effect studies: Rayleigh scattering,
        including molecular parameters for rotational Raman scattering, and the
        Fraunhofer spectrum. Applied Optics 36, 5224 (1997).

        The fit is valid for the 200nm - 1000nm range.

        Parameters
        ----------
        wavenumber : float
           wavenumber (defined as 1/lamda) in cm-1
        ignore_range: bool
           If set to True, it will ignore the valid range of the formula (200nm - 1000nm)

        Returns
        -------
        g : float
           gamma squared in cm^6
        """
        if not ignore_range:
            if (wavenumber > 50000.) or (wavenumber < 10000):
                raise ValueError(
                    "The empirical formula for gamma-squared is valid only between 200nm and 1000nm. Set ignore_range=False if you need to use it anyway.")

        wavenumber_um = wavenumber * 10 ** -4

        g = -6.01466 + 2385.57 / (186.099 - wavenumber_um ** 2)

        g *= 10 ** -25  # Correct scale

        return g ** 2  # Return gamma squared


class O2RamanScattering(MolecularRamanScattering):

    def __init__(self, incident_wavelength, temperature, max_J=40):
        """
        Rotational Raman scattering of N2.

        Parameters
        ----------
        incident_wavelength : float
           Incident wavelength (nm)
        temperature : float
           Ambient temperature (K)
        max_J : int
           Maximum quantum number to use in calculations.
        """
        super(O2RamanScattering, self).__init__(incident_wavelength,
                                                temperature,
                                                name='O_{2}',
                                                B0=1.437682,
                                                D0=4.85E-6,
                                                I=0,
                                                gamma_square=self.gamma_square_function,
                                                g=[0, 1],
                                                relative_concentration=0.21,
                                                max_J=max_J)

    @staticmethod
    def gamma_square_function(wavenumber, ignore_range=False):
        """ Returns the gamma squared parameter for O2 for a given wavelength.

        The empirical fit is take from:

        Chance, K. V. & Spurr, R. J. D. Ring effect studies: Rayleigh scattering,
        including molecular parameters for rotational Raman scattering, and the
        Fraunhofer spectrum. Applied Optics 36, 5224 (1997).

        The fit is valid for the 200nm - 1000nm range.

        Parameters
        ----------
        wavenumber : float
           wavenumber (defined as 1/lamda) in cm-1
        ignore_range: bool
           If set to True, it will ignore the valid range of the formula (200nm - 1000nm)

        Returns
        -------
        g : float
           gamma squared in cm^6
        """
        if not ignore_range:
            if (wavenumber > 50000.) or (wavenumber < 10000):
                raise ValueError(
                    "The empirical formula for gamma-squared is valid only between 200nm and 1000nm. Set ignore_range=False if you need to use it anyway.")

        wavenumber_um = wavenumber * 10 ** -4

        g = 0.07149 + 45.9364 / (48.2716 - wavenumber_um ** 2)

        g *= 10 ** -24  # Correct scale

        return g ** 2  # Return gamma squared



