# Introduction

A script to calculate rotational Raman spectrum of diatomic molecules N2 and O2.

# Installation

You can install the module using the `pip` installation tool e.g.:

```
pip install <local_code_path>
```

# References
Tomasi, C., Vitale, V., Petkov, B., Lupi, A. and Cacciari, A.: Improved
algorithm for calculations of Rayleigh-scattering optical depth in standard
atmospheres, Applied Optics, 44(16), 3320, doi:10.1364/AO.44.003320, (2005)

A. Behrendt and T. Nakamura, "Calculation of the calibration constant of polarization lidar
and its dependency on atmospheric temperature," Opt. Express, vol. 10, no. 16, pp. 805-817, (2002)

# Acknowledgments
Co‐financed by the European Union and Greek national funds
through the Operational Program Competitiveness, Entrepreneurship and Innovation,
under the call RESEARCH – CREATE - INNOVATE (project code:Τ1EDK-02525)

![EU logo](logos/espalogos-eng.png)
