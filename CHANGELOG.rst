Changelog
=========

1.0.0 - 2018-11-20
------------------
* Working code for N2 and O2 molecules.
* Plotting functions for spectra.
* Notebook file with example usage and interactive widget.


