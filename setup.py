#!/usr/bin/env python
import io
import os
import re
from setuptools import setup

# Read the long description from the readme file
with open("README.md", "rb") as f:
    long_description = f.read().decode("utf-8")


# Read the version parameters from the __init__.py file. In this way
# we keep the version information in a single place.
def read(*names, **kwargs):
    with io.open(
            os.path.join(os.path.dirname(__file__), *names),
            encoding=kwargs.get("encoding", "utf8")
    ) as fp:
        return fp.read()


def find_version(*file_paths):
    version_file = read(*file_paths)
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")


# Run setup
setup(name='raman_scattering',
      packages=['raman_scattering', ],
      version=find_version("raman_scattering", "__init__.py"),
      description='Calculation of rotational Raman scattering',
      long_description=long_description,
      author='Raymetrics S.A.',
      author_email='info@raymetrics.com',
      install_requires=[
        "numpy",
        "matplotlib",
        "scipy", 
        "pytest",
        "jupyter",
    ],
      )

